"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms admin *

:details: lara_django_organisms admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_organisms > forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import ExtraData, HabitatClass, HabitatParameterClass, Habitat, FeatureClass, Domain, Regnum, Phylum, Classis, Ordo, Familia, Genus, Species, Subspecies, Variant, Cultivar, Organism


class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            Submit('submit', 'Create')
        )


class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'data_type',
            'namespace',
            'URI',
            'text',
            'XML',
            'JSON',
            'media_type',
            'IRI',
            'URL',
            'description',
            'file',
            Submit('submit', 'Create')
        )


class HabitatClassCreateForm(forms.ModelForm):
    class Meta:
        model = HabitatClass
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'description',
            Submit('submit', 'Create')
        )


class HabitatClassUpdateForm(forms.ModelForm):
    class Meta:
        model = HabitatClass
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'description',
            Submit('submit', 'Create')
        )


class HabitatParameterClassCreateForm(forms.ModelForm):
    class Meta:
        model = HabitatParameterClass
        fields = (
            'name',
            'unit',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'unit',
            'description',
            Submit('submit', 'Create')
        )


class HabitatParameterClassUpdateForm(forms.ModelForm):
    class Meta:
        model = HabitatParameterClass
        fields = (
            'name',
            'unit',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'unit',
            'description',
            Submit('submit', 'Create')
        )


class HabitatCreateForm(forms.ModelForm):
    class Meta:
        model = Habitat
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'description',
            Submit('submit', 'Create')
        )


class HabitatUpdateForm(forms.ModelForm):
    class Meta:
        model = Habitat
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'description',
            Submit('submit', 'Create')
        )


class FeatureClassCreateForm(forms.ModelForm):
    class Meta:
        model = FeatureClass
        fields = (
            'feature',
            'unit',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'feature',
            'unit',
            'description',
            Submit('submit', 'Create')
        )


class FeatureClassUpdateForm(forms.ModelForm):
    class Meta:
        model = FeatureClass
        fields = (
            'feature',
            'unit',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'feature',
            'unit',
            'description',
            Submit('submit', 'Create')
        )


class DomainCreateForm(forms.ModelForm):
    class Meta:
        model = Domain
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'description',
            Submit('submit', 'Create')
        )


class DomainUpdateForm(forms.ModelForm):
    class Meta:
        model = Domain
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'description',
            Submit('submit', 'Create')
        )


class RegnumCreateForm(forms.ModelForm):
    class Meta:
        model = Regnum
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'description',
            Submit('submit', 'Create')
        )


class RegnumUpdateForm(forms.ModelForm):
    class Meta:
        model = Regnum
        fields = (
            'name',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'description',
            Submit('submit', 'Create')
        )


class PhylumCreateForm(forms.ModelForm):
    class Meta:
        model = Phylum
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class PhylumUpdateForm(forms.ModelForm):
    class Meta:
        model = Phylum
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class ClassisCreateForm(forms.ModelForm):
    class Meta:
        model = Classis
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class ClassisUpdateForm(forms.ModelForm):
    class Meta:
        model = Classis
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class OrdoCreateForm(forms.ModelForm):
    class Meta:
        model = Ordo
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class OrdoUpdateForm(forms.ModelForm):
    class Meta:
        model = Ordo
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class FamiliaCreateForm(forms.ModelForm):
    class Meta:
        model = Familia
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class FamiliaUpdateForm(forms.ModelForm):
    class Meta:
        model = Familia
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class GenusCreateForm(forms.ModelForm):
    class Meta:
        model = Genus
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class GenusUpdateForm(forms.ModelForm):
    class Meta:
        model = Genus
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class SpeciesCreateForm(forms.ModelForm):
    class Meta:
        model = Species
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class SpeciesUpdateForm(forms.ModelForm):
    class Meta:
        model = Species
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class SubspeciesCreateForm(forms.ModelForm):
    class Meta:
        model = Subspecies
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class SubspeciesUpdateForm(forms.ModelForm):
    class Meta:
        model = Subspecies
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class VariantCreateForm(forms.ModelForm):
    class Meta:
        model = Variant
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class VariantUpdateForm(forms.ModelForm):
    class Meta:
        model = Variant
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class CultivarCreateForm(forms.ModelForm):
    class Meta:
        model = Cultivar
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Create')
        )


class CultivarUpdateForm(forms.ModelForm):
    class Meta:
        model = Cultivar
        fields = (
            'name',
            'common_name',
            'characteristics',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'characteristics',
            'description',
            Submit('submit', 'Update')
        )


class OrganismCreateForm(forms.ModelForm):
    class Meta:
        model = Organism
        fields = (
            'name',
            'common_name',
            'domain',
            'regnum',
            'phylum',
            'classis',
            'ordo',
            'familia',
            'genus',
            'species',
            'subspecies',
            'variant',
            'cultivar',
            'habitat',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'domain',
            'regnum',
            'phylum',
            'classis',
            'ordo',
            'familia',
            'genus',
            'species',
            'subspecies',
            'variant',
            'cultivar',
            'habitat',
            'description',
            Submit('submit', 'Create')
        )


class OrganismUpdateForm(forms.ModelForm):
    class Meta:
        model = Organism
        fields = (
            'name',
            'common_name',
            'domain',
            'regnum',
            'phylum',
            'classis',
            'ordo',
            'familia',
            'genus',
            'species',
            'subspecies',
            'variant',
            'cultivar',
            'habitat',
            'description')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'common_name',
            'domain',
            'regnum',
            'phylum',
            'classis',
            'ordo',
            'familia',
            'genus',
            'species',
            'subspecies',
            'variant',
            'cultivar',
            'habitat',
            'description',
            Submit('submit', 'Update')
        )

# from .forms import ExtraDataCreateForm, HabitatClassCreateForm, HabitatParameterClassCreateForm, HabitatCreateForm, FeatureClassCreateForm, DomainCreateForm, RegnumCreateForm, PhylumCreateForm, ClassisCreateForm, OrdoCreateForm, FamiliaCreateForm, GenusCreateForm, SpeciesCreateForm, SubspeciesCreateForm, VariantCreateForm, CultivarCreateForm, OrganismCreateFormExtraDataUpdateForm, HabitatClassUpdateForm, HabitatParameterClassUpdateForm, HabitatUpdateForm, FeatureClassUpdateForm, DomainUpdateForm, RegnumUpdateForm, PhylumUpdateForm, ClassisUpdateForm, OrdoUpdateForm, FamiliaUpdateForm, GenusUpdateForm, SpeciesUpdateForm, SubspeciesUpdateForm, VariantUpdateForm, CultivarUpdateForm, OrganismUpdateForm
