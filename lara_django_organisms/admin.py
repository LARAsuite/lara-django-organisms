"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms admin *

:details: lara_django_organisms admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_organisms >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import ExtraData, HabitatClass, HabitatParameterClass, Habitat, FeatureClass, Domain, Regnum, Phylum, Classis, Ordo, Familia, Genus, Species, Subspecies, Variant, Cultivar, Organism


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        'data_type',
        'namespace',
        'URI',
        'text',
        'XML',
        'JSON',
        'bin',
        'media_type',
        'IRI',
        'URL',
        'description',
        'extra_data_id',
        'file',
    )
    list_filter = ('data_type', 'namespace', 'media_type')


@admin.register(HabitatClass)
class HabitatClassAdmin(admin.ModelAdmin):
    list_display = ('habitat_class_id', 'name', 'description')
    search_fields = ('name',)


@admin.register(HabitatParameterClass)
class HabitatParameterClassAdmin(admin.ModelAdmin):
    list_display = (
        'habitat_parameter_class_id',
        'name',
        'unit',
        'description',
    )
    list_filter = ('unit',)
    raw_id_fields = ('extra_data',)
    search_fields = ('name',)


@admin.register(Habitat)
class HabitatAdmin(admin.ModelAdmin):
    list_display = ('habitat_id', 'name', 'description')
    raw_id_fields = ('habitat_classes', 'parameters')
    search_fields = ('name',)


@admin.register(FeatureClass)
class FeatureClassAdmin(admin.ModelAdmin):
    list_display = ('feature_class_id', 'feature', 'unit', 'description')
    list_filter = ('unit',)


@admin.register(Domain)
class DomainAdmin(admin.ModelAdmin):
    list_display = ('domain_id', 'name', 'description')
    search_fields = ('name',)


@admin.register(Regnum)
class RegnumAdmin(admin.ModelAdmin):
    list_display = ('regnum_id', 'name', 'description')
    search_fields = ('name',)


@admin.register(Phylum)
class PhylumAdmin(admin.ModelAdmin):
    list_display = (
        'phylum_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Classis)
class ClassisAdmin(admin.ModelAdmin):
    list_display = (
        'classis_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Ordo)
class OrdoAdmin(admin.ModelAdmin):
    list_display = (
        'tax_order_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Familia)
class FamiliaAdmin(admin.ModelAdmin):
    list_display = (
        'family_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Genus)
class GenusAdmin(admin.ModelAdmin):
    list_display = (
        'genus_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Species)
class SpeciesAdmin(admin.ModelAdmin):
    list_display = (
        'species_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Subspecies)
class SubspeciesAdmin(admin.ModelAdmin):
    list_display = (
        'subspecies_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Variant)
class VariantAdmin(admin.ModelAdmin):
    list_display = (
        'variant_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Cultivar)
class CultivarAdmin(admin.ModelAdmin):
    list_display = (
        'cultivar_id',
        'name',
        'common_name',
        'characteristics',
        'description',
    )
    search_fields = ('name',)


@admin.register(Organism)
class OrganismAdmin(admin.ModelAdmin):
    list_display = (
        'organism_id',
        'name',
        'common_name',
        'domain',
        'regnum',
        'phylum',
        'classis',
        'ordo',
        'familia',
        'genus',
        'species',
        'subspecies',
        'variant',
        'cultivar',
        'habitat',
        'description',
    )
    list_filter = (
        'domain',
        'regnum',
        'phylum',
        'classis',
        'ordo',
        'familia',
        'genus',
        'species',
        'subspecies',
        'variant',
        'cultivar',
        'habitat',
    )
    raw_id_fields = ('features', 'extra_data')
    search_fields = ('name',)
