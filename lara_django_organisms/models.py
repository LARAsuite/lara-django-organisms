"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms models *

:details: lara_django_organisms database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""


import uuid

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models

from lara_django_base.models import ExtraDataAbstr, PhysicalUnit


settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
       e.g. more telephone numbers, customer numbers, ... """

    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(
        upload_to='organisms', blank=True, null=True, help_text="rel. path/filename")


class HabitatClass(models.Model):
    """ classes of habitats, like maritime, terrestrial, shore, ..  """
    habitat_class_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        unique=True, help_text="maritime, terrestrial, shore")
    description = models.TextField(
        blank=True, help_text="description of the habitat class")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''

    class Meta:
        verbose_name_plural = 'HabitatClasses'
        # ~ db_table = "lara_substances_substance_class"


class HabitatParameterClass(models.Model):
    """Habitat Parameter Classes can be used to model all kinds of habitat parameters, 
       like average temperature, moisture, pH, nutrients (?), geological, should be moved to onotolgy

        geo_width_min, geo_width_max 
        temperature_average 
        temperature_min 
        temperature_max 
        oxygen_concentration_min 
        oxygen_concentration_max 
        oxygen_concentration_average
        humidity_average 
        pH_average

        # salt concentration
        moisture_air_min 
        moisture_air_max 
        moisture_air_average
        light_during_day_min
        light_during_day_max
        light_during_day_average 

        height_min
        height_max
    """
    habitat_parameter_class_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)

    name = models.TextField(
        unique=True, help_text="name of the habitat parameter class, e.g. temperature, moisture, pH, ...")

    unit = models.ForeignKey(PhysicalUnit, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_unit_related',
                             related_query_name="%(app_label)s_%(class)s_unit_related_query", blank=True, null=True,
                             help_text="physical unit of the parameter, e.g. K, mol/L, ...")

    extra_data = models.ManyToManyField(ExtraData, related_name='%(app_label)s_%(class)s_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_extra_data_related_query", blank=True,
                                        help_text="e.g. more information about this particular parameter ...")

    description = models.TextField(null=True,
                                   blank=True, help_text="description of the habitat class")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''

    class Meta:
        verbose_name_plural = 'HabitatParameterClasses'
        # ~ db_table = "lara_substances_substance_class"


class Habitat(models.Model):
    """Information about the living conditions of an organism, e.g. temperature, moisture, pH, ... """
    habitat_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        blank=True, null=True, help_text="habitat name with namespaces, like sea.shore, sea.deepsee, land.vulcan")
    habitat_classes = models.ManyToManyField(HabitatClass,  blank=True, related_name="extra_data_addresses",
                                             help_text="class of habitat, e.g. maritim, terrestric, shore, .. ")
    parameters = models.ManyToManyField(HabitatParameterClass,  blank=True, related_name='%(app_label)s_%(class)s_parameters_related',
                                        related_query_name="%(app_label)s_%(class)s_parameters_related_query", help_text="e.g. more parameters, like nutrients, ...")

    description = models.TextField(null=True,
                                   blank=True, help_text="description of the habitat")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''


class FeatureClass(models.Model):
    """ Phenomenological features, like branched, round, oval, .. (GRAM) staining, genetic fingerprint sequence,
       or genetic features, like antibiotic resistance, .. 

    # morphology
    # size
    # shape
    # motility
    # spore
    # flagella
    # capsule

    # growth optimum temp, pH, salt, sugar, ...
    # doubling rate
    # metabolites
    # nutrients
    # metabolic characteristics
    # pathogenicity
    # membrane potential

    """
    feature_class_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    feature = models.TextField(
        unique=True, help_text="Phenomenological features, like branched, round, oval, .. staining, genetic fingerprint sequence ")

    unit = models.ForeignKey(PhysicalUnit, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_unit_related',
                             related_query_name="%(app_label)s_%(class)s_unit_related_query", blank=True, null=True,
                             help_text="physical unit of the parameter, e.g. m, m^2, V, ...")
    description = models.TextField(
        null=True, blank=True, help_text="description of the feature class")

    def __str__(self):
        return self.feature or ''

    def __repr__(self):
        return self.feature or ''

    class Meta:
        verbose_name_plural = 'FeatureClasses'


class Domain(models.Model):
    """ taxonomic domain / super kingdom / empire: Archaea, Bacteria, and Eukarya """
    domain_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        blank=True, null=True, help_text="domain name: Archaea, Bacteria, and Eukarya")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of domain")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''


class Regnum(models.Model):
    """ taxonomic Regnum (Kingdom): Animalia, Plantae, Fungi, Protista, 
        Archaea/Archaebacteria, and Bacteria/Eubacteria 
        might be deprecated 
    """
    regnum_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="Regnum name, like Plantae, Fungi")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of Regnum")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''

    class Meta:
        verbose_name_plural = 'regna'


class Phylum(models.Model):
    """ taxonomic Phylum / Devisio / Division """
    phylum_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="phylum name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common phylum name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of phylum")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of phylum")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''


class Classis(models.Model):
    """ taxonomic Classis / Class """
    classis_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="classis name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common classis name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of this classis")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of this classis")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''


class Ordo(models.Model):
    """ taxonomic Ordo / order  """
    tax_order_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="ordo name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common ordo name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of this ordo")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of this ordo")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''


class Familia(models.Model):
    """ taxonomic Familia / Family """
    family_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="familia name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common familia name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of this familia")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of this familia")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''

    class Meta:
        verbose_name_plural = 'Familia'


class Genus(models.Model):
    """ taxonomic Genus """
    genus_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="genus name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common genus name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of this genus")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of this genus")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''

    class Meta:
        verbose_name_plural = 'Genera'


class Species(models.Model):
    """ taxonomic Species """
    species_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="species name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common species name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of this species")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of this species")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''

    class Meta:
        verbose_name_plural = 'Species'


class Subspecies(models.Model):
    """ taxonomic Subspecies """
    subspecies_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="subspecies name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common subspecies name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of this subspecies")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of this subspecies")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''

    class Meta:
        verbose_name_plural = 'Subspecies'


class Variant(models.Model):
    """ taxonomic Variant   """
    variant_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="variant name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common variant name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of this variant")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of this variant")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''


class Cultivar(models.Model):
    """ taxonomic Cultivar / Plant variety originating from cultivation / cell line """
    cultivar_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(blank=True, null=True,
                            help_text="cultivar name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common cultivar name, like ")
    characteristics = models.TextField(
        blank=True, null=True, help_text="characteristics of this cultivar")
    description = models.TextField(null=True,
                                   blank=True, help_text="description of this cultivar")

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''


class Organism(models.Model):
    """ organism / species / strain  or cell line, details should be in the organism's ontology """
    organism_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.TextField(
        blank=True, null=True, help_text="organism name, like ")
    common_name = models.TextField(
        blank=True, null=True, help_text="common organism name, like ")

    # taxonomy
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_domain_related',
                               related_query_name="%(app_label)s_%(class)s_organism_domain", blank=True, null=True, help_text="organism's domain")

    regnum = models.ForeignKey(Regnum, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_regnum_related',
                               related_query_name="%(app_label)s_%(class)s_organism_regnum", blank=True, null=True, help_text="organism's regnum")
    phylum = models.ForeignKey(Phylum, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_phylum_related',
                               related_query_name="%(app_label)s_%(class)s_organism_phylum", blank=True, null=True, help_text="organism's phylum")
    classis = models.ForeignKey(Classis, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_classis_related',
                                related_query_name="%(app_label)s_%(class)s_organism_classis", blank=True, null=True, help_text="organism's classis")
    ordo = models.ForeignKey(Ordo, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_ordo_related',
                             related_query_name="%(app_label)s_%(class)s_organism_ordo", blank=True, null=True, help_text="organism's ordo")
    familia = models.ForeignKey(Familia, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_familia_related',
                                related_query_name="%(app_label)s_%(class)s_organism_familia", blank=True, null=True, help_text="organism's familia")
    genus = models.ForeignKey(Genus, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_genus_related',
                              related_query_name="%(app_label)s_%(class)s_organism_genus", blank=True, null=True, help_text="organism's genus")
    species = models.ForeignKey(Species, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_species_related',
                                related_query_name="%(app_label)s_%(class)s_organism_species", blank=True, null=True, help_text="organism's species")
    subspecies = models.ForeignKey(Subspecies, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_subspecies_related',
                                   related_query_name="%(app_label)s_%(class)s_organism_subspecies", blank=True, null=True, help_text="organism's subspecies")
    variant = models.ForeignKey(Variant, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_variant_related',
                                related_query_name="%(app_label)s_%(class)s_organism_variant", blank=True, null=True, help_text="organism's variant")
    cultivar = models.ForeignKey(Cultivar, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_cultivar_related',
                                 related_query_name="%(app_label)s_%(class)s_organism_cultivar", blank=True, null=True, help_text="organism's cultivar")

    habitat = models.ForeignKey(Habitat, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_organism_habitat_related',
                                related_query_name="%(app_label)s_%(class)s_organism_habitat", blank=True, null=True, help_text="organism's habitat")

    features = models.ManyToManyField(FeatureClass, related_name='%(app_label)s_%(class)s_organism_feature_related',
                                      related_query_name="%(app_label)s_%(class)s_organism_feature", blank=True, help_text="organism's phenotype features")

    description = models.TextField(
        null=True, blank=True, help_text="description of the organism")

    extra_data = models.ManyToManyField(ExtraData, related_name='%(app_label)s_%(class)s_organism_extradata_related',
                                        related_query_name="%(app_label)s_%(class)s_organism_extradata", blank=True, help_text="organism related extra ")
    
    # def save(self, *args, **kwargs):
    #     """
    #     Here we generate some default values for name_full
    #     """
    #     if not self.name_full:
    #         self.name_full = '/'.join(
    #             (self.namespace.name.strip(), self.name))
    #    super().save(*args, **kwargs)

    def __str__(self):
        return self.name or ''

    def __repr__(self):
        return self.name or ''
