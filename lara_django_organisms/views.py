"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms views *

:details: lara_django_organisms views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

from .forms import OrganismCreateForm, OrganismUpdateForm
from .tables import OrganismTable
from .models import Organism

# Create your  lara_django_organisms views here.


@dataclass
class OrganismsMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [
        {'name': 'Organisms',
         'path': 'lara_django_organisms:organism-list'},
        {'name': 'Organisms-Store',
         'path': 'lara_django_organisms_store:organism-list'},
    ])



class OrganismsSingleTableView(SingleTableView):
    model = Organism
    table_class = OrganismTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'organism_id', 'organism_class', 'shape')

    template_name = 'lara_django_organisms/list.html'
    success_url = '/organisms/organism/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organisms - List"
        context['create_link'] = 'lara_django_organisms:organism-create'
        context['menu_items'] = OrganismsMenu().menu_items
        return context


class OrganismsDetailView(DetailView):
    model = Organism

    template_name = 'lara_django_organisms/organism_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism - Details"
        context['update_link'] = 'lara_django_organisms:organism-update'
        context['menu_items'] = OrganismsMenu().menu_items
        return context


class OrganismsCreateView(CreateView):
    model = Organism

    template_name = 'lara_django_organisms/create_form.html'
    form_class = OrganismCreateForm
    success_url = '/organisms/organism/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism - Create"
        return context


class OrganismsUpdateView(UpdateView):
    model = Organism

    template_name = 'lara_django_organisms/update_form.html'
    form_class = OrganismUpdateForm
    success_url = '/organisms/organism/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism - Update"
        context['delete_link'] = 'lara_django_organisms:organism-delete'
        return context


class OrganismsDeleteView(DeleteView):
    model = Organism

    template_name = 'lara_django_organisms/delete_form.html'
    success_url = '/organisms/organism/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Organism - Delete"
        context['delete_link'] = 'lara_django_organisms:organism-delete'
        return context
