"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_organisms admin *

:details: lara_django_organisms admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_organisms >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_organisms
# generated with django-extensions tests_generator  lara_django_organisms > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import ExtraData, HabitatClass, HabitatParameterClass, Habitat, FeatureClass, Domain, Regnum, Phylum, Classis, Ordo, Familia, Genus, Species, Subspecies, Variant, Cultivar, Organism

class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # name = tables.Column(linkify=('lara_django_organisms:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'bin',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file')

class HabitatClassTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:habitatclass-detail', [tables.A('pk')]))

    class Meta:
        model = HabitatClass

        fields = (
                'name',
                'description')

class HabitatParameterClassTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:habitatparameterclass-detail', [tables.A('pk')]))

    class Meta:
        model = HabitatParameterClass

        fields = (
                'name',
                'unit',
                'description')

class HabitatTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:habitat-detail', [tables.A('pk')]))

    class Meta:
        model = Habitat

        fields = (
                'name',
                'description')

class FeatureClassTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:featureclass-detail', [tables.A('pk')]))

    class Meta:
        model = FeatureClass

        fields = (
                'feature',
                'unit',
                'description')

class DomainTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:domain-detail', [tables.A('pk')]))

    class Meta:
        model = Domain

        fields = (
                'name',
                'description')

class RegnumTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:regnum-detail', [tables.A('pk')]))

    class Meta:
        model = Regnum

        fields = (
                'name',
                'description')

class PhylumTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:phylum-detail', [tables.A('pk')]))

    class Meta:
        model = Phylum

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class ClassisTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:classis-detail', [tables.A('pk')]))

    class Meta:
        model = Classis

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class OrdoTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:ordo-detail', [tables.A('pk')]))

    class Meta:
        model = Ordo

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class FamiliaTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:familia-detail', [tables.A('pk')]))

    class Meta:
        model = Familia

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class GenusTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:genus-detail', [tables.A('pk')]))

    class Meta:
        model = Genus

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class SpeciesTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:species-detail', [tables.A('pk')]))

    class Meta:
        model = Species

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class SubspeciesTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:subspecies-detail', [tables.A('pk')]))

    class Meta:
        model = Subspecies

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class VariantTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:variant-detail', [tables.A('pk')]))

    class Meta:
        model = Variant

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class CultivarTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:cultivar-detail', [tables.A('pk')]))

    class Meta:
        model = Cultivar

        fields = (
                'name',
                'common_name',
                'characteristics',
                'description')

class OrganismTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_organisms:organism-detail', [tables.A('pk')]))

    class Meta:
        model = Organism

        fields = (
                'name',
                'common_name',
                'domain',
                'regnum',
                'phylum',
                'classis',
                'ordo',
                'familia',
                'genus',
                'species',
                'subspecies',
                'variant',
                'cultivar',
                'habitat',
                'description')

