Welcome to LARA-django Organisms's documentation!
======================================================================

.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   source/modules
   development
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
